module System.StartTorrent where

import           RIO
import           RIO.Process

import           Lens.Micro                     ( (?~) )

import           Control.Exception              ( AsyncException(..) )
import           Options.Applicative

import           AppOptions
import           Data.App
import           Data.Config
import           Data.Logging
import           Protocol.PeerID
import           Protocol.Torrent
import           System.Piece


main :: IO ()
main = do
  pid        <- generateID
  options    <- (aConfigL ?~ "./test/System/default.conf") <$> execParser opt
  processCtx <- mkDefaultProcessContext
  let minLogLevel = case options ^. verboseL of
        Just True -> LevelDebug
        _         -> LevelInfo
      lo = defaultOpts { _logLevel = minLogLevel }
  config <- getConfig options >>= \case
    Just c  -> pure c
    Nothing -> error "Couldn't generate config"
  withHLog lo $ \lf -> do
    let app = App { _logFunc    = lf
                  , _processCtx = processCtx
                  , _config     = config
                  , _logOpts    = lo
                  }
        runApp = do
          (fileA, rQ, wQ) <- startFileThread lo
          appA <- runRIO app $ runTorrent (_file options) pid rQ wQ startTorrent
          pure [appA, fileA]
    void $ bracket runApp (mapM_ (`cancelWith` UserInterrupt)) waitAnyCancel
