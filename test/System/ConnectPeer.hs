module System.ConnectPeer where

import           Prelude                        ( print )
import           RIO                     hiding ( logDebug
                                                , logInfo
                                                , logWarn
                                                , logError
                                                )
import           RIO.List.Partial
import           RIO.Process

import           Control.Exception              ( AsyncException(..) )
import           Data.Array
import           Data.Array.MArray

import           AppOptions
import           Data.App
import           Data.Config
import           Data.Logging
import           Data.Piece
import           Data.Progress
import           Data.MetaInfo
import           Data.Torrent
import           Protocol.PeerID
import           Protocol.Torrent
import           System.Piece

tryGetPiece :: TorrentStack Int
tryGetPiece = bracket
  logic
  (\t -> do
    env <- ask
    let name = env ^. metaInfoL . infoDictL . nameL
    logWarn $ "Cancelling torrent " <> displayBytesUtf8 name
    saveProgress
    cancel t
  )
  wait
 where
  logic = async $ do
    loadProgress
    env <- ask
    (atomically . freeze $ env ^. pieceBitFieldL)
      >>= (logWarn . displayShow @(Array Word32 PieceState))
    _   <- trackerRefresh
    mtr <- readTVarIO (env ^. trackerInfoL)
    tr  <- case mtr of
      Just x  -> return x
      Nothing -> error "No tracker response"
    let ps = _peerAddrs tr
    liftIO $ print ps
    t <- connectPeer (ps !! 1)
    wait t
    return 0

defOpt :: AppOptions
defOpt = AppOptions
  { _verbose    = Just True
  , _sessionDir = Nothing
  , _file       =
    "test/torrent-files/torrents/archlinux-2019.08.01-x86_64.iso.torrent"
  , _aConfig    = Just ".test/System/default.conf"
  }

main :: IO ()
main = do
  pid        <- generateID
  processCtx <- mkDefaultProcessContext
  config     <- getConfig defOpt >>= \case
    Just c  -> pure c
    Nothing -> error "Couldn't generate config"
  void $ withHLog defaultOpts $ \lf -> do
    let
      app = App { _logFunc    = lf
                , _processCtx = processCtx
                , _config     = config
                , _logOpts    = defaultOpts
                }
      runApp = do
        (_, rQ, wQ) <- startFileThread defaultOpts
        runRIO app $ runTorrent
          "test/torrent-files/torrents/archlinux-2019.08.01-x86_64.iso.torrent"
          pid
          rQ
          wQ
          tryGetPiece
    bracket runApp (`cancelWith` UserInterrupt) wait
