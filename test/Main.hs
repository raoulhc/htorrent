module Main where

import           RIO
import           RIO.Directory

import           Test.Hspec

import qualified Spec

main :: IO ()
main =
  hspec
    . before_ (removePathForcibly "./.tmp/" >> createDirectory "./.tmp")
    $ Spec.spec
