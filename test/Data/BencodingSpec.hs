{-# LANGUAGE OverloadedStrings #-}

module Data.BencodingSpec
  ( spec
  )
where

import           RIO
import qualified RIO.Vector                    as V
import qualified RIO.Map                       as M
import           Data.Serialize
import           Test.Hspec

import           Data.Bencoding

spec :: Spec
spec = do
  describe "bInt" $ do
    it "i1234e" $ do
      let i = runGet bInt "i1234e"
      i `shouldBe` Right (BInt 1234)
    it "i123fe" $ do
      let i = runGet bInt "i123fe"
      isLeft i `shouldBe` True

  describe "bStr" $ do
    it "4:spam" $ do
      let i = runGet bStr "4:spam"
      i `shouldBe` Right (BStr "spam")
    it "8:testingabcd" $ do
      let i = runGet bStr "8:testingabcd"
      i `shouldBe` Right (BStr "testinga")
    it "13:uTorrent/2210" $ do
      let i = runGet bStr "13:uTorrent/2210"
      i `shouldBe` Right (BStr "uTorrent/2210")

  describe "bList" $ do
    it "l4:spame" $ do
      let i = runGet bList "l4:spame"
      i `shouldBe` Right (BList (V.fromList [BStr "spam"]))
    it "l4:spam4:eggs:l5:baconee" $ do
      let i = runGet bList "l4:spam4:eggsl5:baconee"
      i `shouldBe` Right
        (BList
          (V.fromList
            [BStr "spam", BStr "eggs", BList (V.fromList [BStr "bacon"])]
          )
        )

  describe "bDict"
    $ it "d8:announce42:http://tracker.archlinux.org:6969/announcee"
    $ do
        let
          i = runGet
            bDict
            "d8:announce42:http://tracker.archlinux.org:6969/announcee"
        i `shouldBe` Right
          ( BDict
          . M.fromList
          $ [("announce", BStr "http://tracker.archlinux.org:6969/announce")]
          )
