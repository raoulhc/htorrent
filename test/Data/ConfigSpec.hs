module Data.ConfigSpec where

import           RIO
import           RIO.Directory
import           RIO.FilePath
import           Data.Monoid

import           Test.Hspec

import           AppOptions
import           Data.Config
import           Utils                          ( expandHome )

spec :: Spec
spec = describe "Config tests" $ do
  it "read builder" $ do
    cb <- readConfig "./test/Data/TestConfig1"
    cb `shouldBe` Just
      (ConfigBuilder (First $ Just "~/.htd/session")
                     (First Nothing)
                     (First Nothing)
                     (First Nothing)
      )

  it "combine builders" $ do
    cb1 <- readConfig "./test/Data/TestConfig1"
    cb2 <- readConfig "./test/Data/TestConfig2"
    cb1 <> cb2 `shouldBe` Just
      (ConfigBuilder (First $ Just "~/.htd/session")
                     (First Nothing)
                     (First $ Just "~/.htd/log2")
                     (First Nothing)
      )
    cb3 <- readConfig "./test/Data/TestConfig3"
    cb1 <> cb2 <> cb3 `shouldBe` Just
      (ConfigBuilder (First $ Just "~/.htd/session")
                     (First $ Just "~/.htd/downloads3")
                     (First $ Just "~/.htd/log2")
                     (First Nothing)
      )

  it "construct builder" $ do
    home <- getHomeDirectory
    let ex     = expandHome home
        mvOld  = renamePath (ex "~/.htd/config") (ex "~/.htd/config.tmp")
        cpNew  = copyFile (ex "./test/Data/TestConfig3") (ex "~/.htd/config")
        mvBack = renamePath (ex "~/.htd/config.tmp") (ex "~/.htd/config")
    bracket mvOld (const mvBack) $ const $ do
      cpNew
      let app = AppOptions Nothing
                           Nothing
                           (Just "./test/Data/TestConfig2")
                           "doen't matter"
      cfg <- getConfig app
      cfg `shouldBe` Just
        (Config (home </> ".htd/session2")
                (home </> ".htd/downloads3")
                (Just $ home </> ".htd/log2")
                False
        )
