{-# LANGUAGE OverloadedStrings #-}

module Data.MessageSpec where

import           RIO
import           Data.Serialize
import           Test.Hspec

import           Data.Message
import           Data.Piece

checkGet :: Message -> ByteString -> Expectation
checkGet m bs = do
  let msg = runGet get bs
  msg `shouldBe` Right m

checkPut :: Message -> ByteString -> Expectation
checkPut m bs = do
  let msg = runPut (put m)
  msg `shouldBe` bs

checkPutGet :: Message -> ByteString -> Spec
checkPutGet m bs = do
  it "get" $ checkGet m bs
  it "put" $ checkPut m bs

spec :: Spec
spec = do
  describe "keep alive" $ checkPutGet KeepAlive "\0\0\0\0"
  describe "Choke" $ checkPutGet Choke "\0\0\0\1\0"
  describe "Unchoke" $ checkPutGet Unchoke "\0\0\0\1\1"
  describe "Interested" $ checkPutGet Interested "\0\0\0\1\2"
  describe "Uninterested" $ checkPutGet Uninterested "\0\0\0\1\3"
  describe "Have" $ checkPutGet (Have (2 :: Word32)) "\0\0\0\5\4\0\0\0\2"
  describe "Bitfield" $ do
    it "put1" $ checkPut (Bitfield "\152") "\0\0\0\2\5\152"
    it "put2" $ checkPut (Bitfield "\0\0\0\152") "\0\0\0\5\5\0\0\0\152"
    it "get" $ checkGet (Bitfield "\152\152\128") "\0\0\0\4\5\152\152\128"
  describe "Request" $ checkPutGet (Request (PieceLoc 21 73) 24)
                                   "\0\0\0\13\6\0\0\0\21\0\0\0\73\0\0\0\24"
  describe "Piece" $ checkPutGet (Piece (PieceLoc 2 201) "\0\0\0\240")
                                 "\0\0\0\13\7\0\0\0\2\0\0\0\201\0\0\0\240"
  describe "Cancel" $ checkPutGet (Cancel (PieceLoc 250 7) 8)
                                  "\0\0\0\13\8\0\0\0\250\0\0\0\7\0\0\0\8"
  -- describe "Port" $ undefined
