module Utils.BShow where

import           RIO
import qualified RIO.ByteString                as B
import qualified Data.ByteString.Char8         as BC

import           Numeric                        ( showHex )

import           Data.Message
import           Data.Piece

-- | Class for showing data messages as bytestrings, formatting raw data in hex
class BShow a where
  bshow :: a -> ByteString

instance BShow Message where
  bshow (Have     ix    ) = "Have 0x" <> BC.pack (showHex ix "")
  bshow (Bitfield bs    ) = "Bitfield " <> bsHex bs
  bshow (Request loc len) = mconcat ["Request ", bshow loc, "-", bshow len]
  bshow (Piece   loc bs ) = mconcat ["Piece ", bshow loc, ": ", bsHex bs]
  bshow x                 = BC.pack . show $ x

instance BShow PieceLoc where
  bshow (PieceLoc loc offset) =
    mconcat ["loc (", bshow loc, "-", bshow offset, ")"]

instance BShow ByteString where
  bshow = bsHex

instance BShow Word32 where
  bshow = BC.pack . flip showHex ""

-- | Print bytestring in hex format prepended with "0x"
bsHex :: ByteString -> ByteString
bsHex = ("0x" <>) . B.concatMap (BC.pack . flip showHex "")
