{-|
Module      : Protocol.PeerID
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Method for generating torrent ID's randomly
-}

module Protocol.PeerID where

import           RIO
import qualified RIO.ByteString                as B
import           System.Random

-- | Randomly generate an id, for use at the beginning of the session
generateID :: IO ByteString
generateID = do
  let prefix = "-HT0000-"
  g <- newStdGen
  let peerid = take 12 $ randomRs (0 :: Word8, 255) g
  return $ prefix <> B.pack peerid
