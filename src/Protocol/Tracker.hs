{-|
Module      : Protocol.Tracker
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Methods involving tracker requests
-}

module Protocol.Tracker where

import           RIO
import qualified RIO.ByteString                as B
import           RIO.Partial
import           Control.Lens                   ( (^?) )
import qualified Data.ByteString.Char8         as BC
import           Data.ByteString.Char8          ( ByteString )
import           Data.ByteString.Lazy           ( toStrict )
import           Data.Either.Combinators
import qualified Data.Serialize                as S
import           Control.Exception
import           Network.Socket
import           Network.Wreq

import           Data.Bencoding
import           Data.MetaInfo
import           Data.UrlEncode
import           Data.Torrent

-- | Potential exceptions raised for a tracker
data TrackerException
  = ResponseError String
  | InvalidIntervalKey
  | InvalidCompleteKey
  | InvalidIncompleteKey
  | InvalidPeerList
  deriving Show

instance Exception TrackerException

-- TODO need to gradually add various options do as needs, will need to get
-- several values out of it as
-- | create a url out of a metainfo structure
getTrackerURL
  :: ByteString   -- ^ peer_id
  -> MetaInfo     -- ^ metainfo
  -> ByteString   -- ^ tracker url to request
getTrackerURL pid mi = url <> "?" <> urlEncodeVars
  [ ("info_hash" , _infoHash mi)
  , ("peer_id"   , pid)
  , ("port"      , str 6881)
  , ("uploaded"  , str 0)
  , ("downloaded", str 0)
  , ("left"      , str 0)
  , ("compact"   , str 1)
  ]
 where
  url = _announce mi
  str :: Int -> ByteString
  str = BC.pack . show

-- | Perform tracker request, verify and return response data struct
trackerRequest :: ByteString -> IO (Either String TrackerResponse)
trackerRequest url = do
  r <- liftIO $ get . BC.unpack $ url
  let rs = r ^. responseStatus
  if rs ^. statusCode /= 200
    then throw . ResponseError $ "Error message: " <> BC.unpack
      (rs ^. statusMessage)
    else return $ verifyTrackerResponse (toStrict $ r ^. responseBody)

-- | verify the tracker response has necessary info and return response
--   data struct
verifyTrackerResponse :: ByteString -> Either String TrackerResponse
verifyTrackerResponse bs = do
  bc       <- S.decode bs :: Either String Bencoding
  interval <- maybeToRight "No valid interval key given"
                           (bc ^? key "interval" . _BInt)
  let minInterval = bc ^? key "min interval" . _BInt
  let seeders     = bc ^? key "complete" . _BInt
  let leechers    = bc ^? key "incomplete" . _BInt
  peers <- maybeToRight "Binary peer list not a multiple of 6"
                        (bc ^? key "peers" . _BStr >>= binaryPeers)
  return $ TrackerResponse interval peers minInterval seeders leechers

-- | decodes the compact form of peers into useful data structures
binaryPeers :: ByteString -> Maybe [SockAddr]
binaryPeers bps = if B.length bps `mod` 6 == 0
  then Just $ getPeers bps
  else Nothing
 where
  getPeers "" = []
  getPeers bp = getPeer b : getPeers bs where (b, bs) = B.splitAt 6 bp
  getPeer peer = case B.unpack peer of
    [a, b, c, d, x, y] -> SockAddrInet
      (toEnum $ fromEnum x * 256 + fromEnum y)
      (tupleToHostAddress (a, b, c, d))
    _ -> throw $ PatternMatchFail "Corrupted message sent"

-- | does a scrape request
scrapeRequest :: ByteString -> IO (Either String Bencoding)
scrapeRequest url = do
  r <- get . BC.unpack $ url
  return . S.decode . toStrict $ r ^. responseBody
