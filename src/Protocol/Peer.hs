{-|
Module      : Protocol.Peer
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains methods used by connections to an individual peer, as well as
methods to start a peer connection and start threads that use these methods
(send, receive and timeout threads).
-}

module Protocol.Peer where

import           RIO                     hiding ( logError
                                                , logWarn
                                                , logDebug
                                                , logInfo
                                                )
import           RIO.List                       ( genericIndex )
import           RIO.Partial
import qualified RIO.ByteString                as B

import           Lens.Micro                     ( (%~) )

import           Data.Array                     ( Array
                                                , elems
                                                )
import           Data.Array.MArray
import           Control.Concurrent.STM.TSem
import           Control.Exception
import           Crypto.Hash.SHA1
import           Network.Socket                 ( Socket
                                                , SockAddr
                                                )
import           System.Random

import           Data.Bitfield
import           Data.MetaInfo
import           Data.Message
import           Data.Logging
import           Data.Piece
import           Data.Peer
import           Data.Torrent
import           Protocol.Messages
import           System.Piece

import           Utils.BShow                    ( bshow )

-- | Method to set up peer monad and run an action
runPeer
  :: MonadIO m
  => TorrentEnv
  -> MVar ()
  -> Maybe ByteString
  -> Socket
  -> SockAddr
  -> PeerMonad a
  -> m a
runPeer torEnv finSig pid s addr peerMonad = do
  tpid          <- newTVarIO pid
  amChoke       <- newTVarIO False
  amInterested  <- newTVarIO False
  pamChoke      <- newTVarIO True
  pamInterested <- newTVarIO False
  pTimeout      <- newTVarIO defaultTimeout
  bounds        <- atomically . getBounds $ torEnv ^. pieceBitFieldL
  pPieceList    <- atomically $ newArray bounds False
  outReqs       <- atomically $ newTSem 10
  inReqs        <- newTBQueueIO 20
  let lo  = torEnv ^. logOptionsL
      lo' = lo & contextL %~ (<> [(black, show addr)])
  withHLog lo' $ \lf -> do
    let initR = PeerEnv torEnv
                        tpid
                        s
                        amChoke
                        amInterested
                        pamChoke
                        pamInterested
                        pTimeout
                        pPieceList
                        outReqs
                        inReqs
                        finSig
                        lf
    runRIO initR (runPeerMonad peerMonad)

-- TODO This will have to be updated to be more generic rather than aiming for
-- solely downloading. Also remove specific code for setting it up. Should
-- eventually be just a handshake and setting up the threads.
-- | Start the peer for downloading
startPeer :: PeerMonad ()
startPeer = do
  pTimeout <- view timeoutL
  aFinish  <- async finishThread
  aThreads <- mapM async [receiveThread, sendThread, timeoutThread pTimeout]
  (a, res) <- waitAnyCatch $ aFinish : aThreads
  unless (a == aFinish) $ case res of
    Right () -> void $ waitAnyCatch aThreads
    Left  e  -> logError $ "Thread failed: " <> displayShow e

-- | Finish
finishThread :: (MonadReader PeerEnv m, MonadIO m) => m ()
finishThread = do
  sig <- view pFinishSigL
  takeMVar sig
  int <- view pInterestedL >>= readTVarIO
  if int then return () else throw PeerFinished

-- | Thread that just times out
timeoutThread :: MonadIO m => TVar Word32 -> m ()
timeoutThread pTimeout = do
  threadDelay 1000000 -- wait second between decrementing
  atomically $ modifyTVar' pTimeout (\x -> x - 1)
  current <- readTVarIO pTimeout
  when (current > maxBound - 10) $ throw PeerTimeout
  timeoutThread pTimeout

-- | Thread with logic for listening to peer messages
receiveThread :: (MonadIO m, MonadReader PeerEnv m) => m ()
receiveThread = do
  msg <- receive
  respond msg
  -- reset timer when any message is received
  pTimeout <- view timeoutL
  atomically $ writeTVar pTimeout defaultTimeout
  receiveThread

-- Logic to respond to messages
respond :: (MonadIO m, MonadReader PeerEnv m) => Message -> m ()
respond msg = do
  logDebug . displayBytesUtf8 . B.take 40 $ bshow msg
  case msg of
    KeepAlive                -> return ()
    Choke                    -> changeState pChokeL True
    Unchoke                  -> changeState pChokeL False
    Interested               -> changeState pInterestedL True >> updateChoke
    Uninterested             -> changeState pInterestedL False
    Have     x               -> addPiece x >> updateInterest
    Bitfield bs              -> addBitfield bs >> updateInterest
    Request loc      len     -> respondReq loc len
    Piece   pieceLoc payload -> respondPiece pieceLoc payload
    _                        -> do
      logError . displayShow $ "Unimplemented message sent: " <> bshow msg
      throw Unimplemented
 where
  changeState f newState = do
    state <- view f
    atomically $ writeTVar state newState

-- | Thread with logic for sending peer messages
sendThread :: (MonadIO m, MonadReader PeerEnv m) => m ()
sendThread = do
  r <- reqTrue
  when r sendReq
  inReqs <- view inReqsL
  req    <- atomically $ tryReadTBQueue inReqs
  case req of
    Just (loc, len) ->
      logDebug ("Sending req of " <> displayShow req) *> piece loc len
    Nothing -> unless r $ threadDelay 100000
  sendThread
 where
  reqTrue = do
    intT          <- view amInterestedL
    pChokeT       <- view pChokeL
    (int, pChoke) <- atomically $ do
      int    <- readTVar intT
      pChoke <- readTVar pChokeT
      return (int, pChoke)
    return $ int && not pChoke

-- Send requests messages for a random piece
sendReq :: (MonadIO m, MonadReader PeerEnv m) => m ()
sendReq = do
  -- Select random piece we don't have that the peer has
  peerBitfield <- view pPieceListL
  pieces <- fmap fst . filter snd <$> (atomically . getAssocs $ peerBitfield)
  ix           <- liftIO $ randomRIO (0, length pieces - 1)
  mi           <- fmap (^. metaInfoL) getEnv
  let p         = pieces `genericIndex` ix
      pieceReqs = pieceMsgs mi p
  logInfo $ "Requesting piece: " <> displayShow p
  masterBitfield <- fmap (^. pieceBitFieldL) getEnv
  atomically $ do
    writeArray masterBitfield p $ Downloading (toEnum $ length pieceReqs)
    writeArray peerBitfield p False
  mapM_ (uncurry request) pieceReqs
  when (length pieces == 1) $ do
    amInterested <- view amInterestedL
    atomically $ writeTVar amInterested False

updateInterest :: (MonadIO m, MonadReader PeerEnv m) => m ()
updateInterest = do
  env          <- ask
  amInterested <- readTVarIO (env ^. amInterestedL)
  unless amInterested $ do
    masterBitfield :: Array Word32 PieceState <-
      atomically . freeze $ env ^. torrentRL . pieceBitFieldL
    peerBitfield :: Array Word32 Bool <-
      atomically . freeze $ env ^. pPieceListL
    let something = zip (elems masterBitfield) (elems peerBitfield)
    when (any (\(x, y) -> x == Lack && y) something) $ do
      atomically $ writeTVar (env ^. amInterestedL) True
      interested
      logDebug "Sent interested message"

updateChoke :: (MonadIO m, MonadReader PeerEnv m) => m ()
updateChoke = do
  pChoke <- view pChokeL
  test   <- readTVarIO pChoke
  atomically . writeTVar pChoke $ False
  when test unchoke

-- | Parse bytestring and update peer bitfield
addBitfield :: (MonadIO m, MonadReader PeerEnv m) => ByteString -> m ()
addBitfield bs = do
  pieceList <- view pPieceListL
  updateBitfield True bs pieceList
  -- mark ones we already have
  masterBitfield <- view $ torrentRL . pieceBitFieldL
  (x, y)         <- atomically $ getBounds pieceList
  forM_ [x .. y] $ \ix -> do
    got <- atomically $ readArray masterBitfield ix
    when (got /= Lack) $ atomically $ writeArray pieceList ix False

-- | Response to piece messages to keep track of pieces a peer has that we want
addPiece :: (MonadIO m, MonadReader PeerEnv m) => Word32 -> m ()
addPiece y = do
  masterPieceList <- view $ torrentRL . pieceBitFieldL
  state           <- atomically $ readArray masterPieceList y
  -- keep track of pieces we don't have
  when (state == Lack) $ do
    pieceList <- view pPieceListL
    atomically $ writeArray pieceList y True

-- | add request to pipeline if there's space.
respondReq :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> Word32 -> m ()
respondReq loc len = do
  amChoke     <- view amChokeL >>= readTVarIO
  pInterested <- view pInterestedL >>= readTVarIO
  when (not amChoke && pInterested) $ do
    inReqs <- view inReqsL
    atomically $ writeTBQueue inReqs (loc, len)

-- | Respond to a piece message
respondPiece
  :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> ByteString -> m ()
respondPiece pl@(PieceLoc ix _) payload = do
  -- decrease semaphore
  outReqs <- view outReqsL
  atomically $ signalTSem outReqs
  -- decrease blocks left in master
  writeBlockM pl payload
  masterBitfield <- view $ torrentRL . pieceBitFieldL
  done           <- atomically $ do
    state <- readArray masterBitfield ix
    case state of
      Downloading x -> if x /= 1
        then do
          writeArray masterBitfield ix (Downloading $ x - 1)
          return False
        else return True
      _ -> throw $ UnrequestedPiece ix
  finished <- view $ torrentRL . finishedL
  when done $ do
    peerBitfield <- view pPieceListL
    match        <- hashPiece pl
    atomically $ do
      writeArray masterBitfield ix $ if match then Got else Lack
      unless match $ writeArray peerBitfield ix True
    count <- pieceStatus match
    logInfo $ "Pieces left: " <> displayShow count
  fin <- readTVarIO finished
  when fin finishedState

-- Check whether we've finished downloading the torrent optional decrementing
-- the piece count
pieceStatus
  :: (MonadIO m, HasTorrentEnv env, MonadReader env m) => Bool -> m Word32
pieceStatus decr = do
  env <- getEnv
  let pieces = env ^. piecesLeftL
  count <- if decr
    then atomically $ do
      count <- readTVar pieces
      writeTVar pieces $ count - 1
      return $ count - 1
    else readTVarIO pieces
  when (count == 0) $ atomically . flip writeTVar True $ env ^. finishedL
  return count

-- | Update state once a torrent has finished
finishedState :: (MonadReader PeerEnv m, MonadIO m) => m ()
finishedState = do
  amInterested <- view amInterestedL
  atomically $ writeTVar amInterested False

hashPiece :: (MonadReader PeerEnv m, MonadIO m) => PieceLoc -> m Bool
hashPiece (PieceLoc ix _) = do
  len       <- view $ torrentRL . metaInfoL . infoDictL . pieceLengthL
  pieceHash <- hash <$> readBlockM (PieceLoc ix 0) len
  -- find expected piece hash
  pieces    <- view $ torrentRL . metaInfoL . infoDictL . piecesL
  let expectedHash = B.take 20 . B.drop (fromEnum ix * 20) $ pieces
  if pieceHash == expectedHash
    then return True
    else do
      logWarn $ "Hash didn't match for piece " <> displayShow ix
      return False
