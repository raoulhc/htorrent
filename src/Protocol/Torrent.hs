{-|
Module      : Protocol.Torrent
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Methods for runnings threads and making peer connections.
-}

module Protocol.Torrent where

-- import Prelude (print)
import           RIO                     hiding ( logDebug
                                                , logInfo
                                                , logWarn
                                                , logError
                                                )
import qualified Data.ByteString.Char8         as BC
import qualified RIO.HashMap                   as H
import           RIO.List
import           Lens.Micro                     ( (%~) )

import           Control.Monad.STM              ( retry )
import           Data.Array.MArray
import           Network.Socket          hiding ( recv )
import           Text.Printf
import           Crypto.Hash.SHA1               ( hash )
import           Control.Exception              ( throw
                                                , IOException
                                                )

import           Data.App
import           Data.Config
import           Data.Logging
import           Data.MetaInfo
import           Data.Torrent
import           Data.Piece
import           Data.Progress
import           Protocol.Tracker
import           Protocol.Messages              ( outHandshake
                                                , inHandshake
                                                )
import           Protocol.Peer
import           System.Piece

-- | Exceptions raised at the torrent level
data TorrentException
  = InvalidTorrentFile
  | TrackerError
  deriving Show

instance Exception TorrentException

-- | Runs a torrent monad given initial conditions
runTorrent
  :: FilePath         -- ^ torrent location
  -> ByteString       -- ^ PeerID
  -> ReadQueue
  -> WriteQueue
  -> TorrentStack a   -- ^ Torrent stack to execute
  -> RIO App (Async a)
runTorrent torrentFP peerID rQ wQ tStack = do
  result <- readTorrent torrentFP
  let metainfo = case result of
        Left  _ -> throw InvalidTorrentFile
        Right x -> x
  config      <- view configL
  trackerInfo <- newTVarIO Nothing
  downloadFP  <- newTVarIO $ config ^. cfgDownloadPathL
  let size = _pieceNumber metainfo
  pieceBitField  <- atomically $ newArray (0, size - 1) Lack
  piecesLeft     <- newTVarIO size
  peerSockets    <- newTVarIO H.empty
  connectedPeers <- newTVarIO 0
  finished       <- newTVarIO False
  opts           <- view logOptsL
  let opts' =
        opts
          &  contextL
          %~ (<> [(green, BC.unpack $ metainfo ^. infoDictL . nameL)])
  withHLog opts' $ \lf -> do
    let initR = TorrentEnv config
                           metainfo
                           peerID
                           trackerInfo
                           downloadFP
                           pieceBitField
                           piecesLeft
                           finished
                           peerSockets
                           connectedPeers
                           lf
                           opts'
                           rQ
                           wQ
    async (runRIO initR (runTorrentStack tStack))

-- | send a tracker request and update tracker state
trackerRefresh :: (MonadIO m, MonadReader TorrentEnv m) => m Bool
trackerRefresh = do
  logDebug "Querying tracker"
  env     <- ask
  mi      <- view metaInfoL
  lPeerId <- view peerIDL
  let url = getTrackerURL lPeerId mi
  trackerResponse <- view trackerInfoL
  etr             <- liftIO $ trackerRequest url
  let writeResponse = atomically . writeTVar trackerResponse
  case etr of
    Left e -> do
      logDebug $ "Failed to connect to tracker: " <> displayShow e
      writeResponse Nothing >> return False
    Right tr -> do
      logDebug $ "Got tracker response" <> displayShow tr
      writeResponse (Just tr)
      peers    <- readTVarIO (env ^. peersL)
      newPeers <- forMaybeM (tr ^. peerAddrsL) $ \addr ->
        if not $ H.member addr peers
          then do
            finSig <- newEmptyMVar
            return $ Just (addr, PeerInfo Nothing Nothing False False finSig)
          else return Nothing
      atomically . modifyTVar' (env ^. peersL) $ flip H.union
                                                      (H.fromList newPeers)
      return True

-- | Start the thread logic for a torrent
startTorrent :: (MonadUnliftIO m, MonadIO m, MonadReader TorrentEnv m) => m ()
startTorrent = bracket loadProgress (const saveProgress) runLogic where
  runLogic = const $ do
    update <- trackerRefresh
    count  <- pieceStatus False
    unless update $ throw TrackerError
    download <- if count /= 0
      then Just <$> async connectPeers
      else return Nothing
    if count /= 0 then void $ async finishLogic else pure ()
    aListen <- async listenForPeers
    aUpdateTracker <- async updateTrackerLogic
    res <- waitAnyCatch $ [aListen, aUpdateTracker] <> catMaybes [download]
    logWarn . displayShow $ snd res
    return ()

-- | Logic for thread refreshing the tracker
updateTrackerLogic :: (MonadIO m, MonadReader TorrentEnv m) => m ()
updateTrackerLogic = do
  mtr <- view trackerInfoL >>= readTVarIO
  case mtr of
    Just tr -> waitSecs $ fromEnum (tr ^. intervalL)
    Nothing -> waitSecs 900
  _ <- trackerRefresh
  updateTrackerLogic
  where waitSecs sec = threadDelay $ 1000000 * sec

-- | Logic that listens for download finish
finishLogic :: (MonadIO m, MonadReader TorrentEnv m) => m ()
finishLogic = do
  finishedT <- view finishedL
  atomically $ do
    newFinish <- readTVar finishedT
    unless newFinish retry
  -- Should only get here if finished
  hashGood <- hashAll
  if hashGood
    then
      logInfo "Successfully finished downloading"
        >> closeUninterestedConnections
    else logError "Finished downloading but hashes incorrectly"

closeUninterestedConnections :: (MonadIO m, MonadReader TorrentEnv m) => m ()
closeUninterestedConnections = do
  peers <- view peersL >>= readTVarIO
  mapM_ sendFinishSignal peers
  where sendFinishSignal p = let sig = p ^. piSignalL in putMVar sig ()

-- | Hash all the pieces to double check they're correct
hashAll :: (MonadReader TorrentEnv m, MonadIO m) => m Bool
hashAll = do
  pieceHashes <- view $ metaInfoL . infoDictL . piecesL
  pieceNum    <- view $ metaInfoL . pieceNumberL
  pieceLength <- view $ metaInfoL . infoDictL . pieceLengthL
  let f ctx ix = (ctx <>) . hash <$> readBlockM (PieceLoc ix 0) pieceLength
  hashes <- foldM f "" [0, 1 .. pieceNum - 1]
  pure $ pieceHashes == hashes

-- | Connect to multiple peers
connectPeers :: (MonadUnliftIO m, MonadIO m, MonadReader TorrentEnv m) => m ()
connectPeers = do
  logInfo "Connecting to peers"
  addrs <- topUpPeers
  as    <- mapM connectPeer addrs
  peerLogic as

-- | Logic for reconnecting to peers when other connections drop
peerLogic
  :: (MonadUnliftIO m, MonadIO m, MonadReader TorrentEnv m)
  => [Async ()]
  -> m ()
peerLogic as = do
  a     <- fst <$> waitAnyCatch as
  addrs <- topUpPeers
  nas   <- mapM connectPeer addrs
  peerLogic $ nas ++ delete a as

-- | If we're below amount of peers we want return list of peers to connect to
topUpPeers :: (MonadIO m, MonadReader TorrentEnv m) => m [SockAddr]
topUpPeers = do
  env            <- ask
  connectedCount <- readTVarIO $ env ^. connectedPeersL
  logDebug
    . displayShow
    $ (printf "Connected to %i/%i peers" connectedCount maxPeers :: String)
  if connectedCount < maxPeers
    then atomically $ do
      peers <- readTVar $ env ^. peersL
      let addrs = chooseConnections (maxPeers - connectedCount) peers
      forM_ addrs $ modifyTVar' (env ^. peersL) . H.adjust (set piInUseL True)
      return addrs
    else return []

-- TODO might want to update to be based on speed or something
-- | Pick n currently unconnected peers to from trackers peer list
chooseConnections :: Int -> HashMap SockAddr PeerInfo -> [SockAddr]
chooseConnections num ps = snd $ H.foldlWithKey' handler (num, []) ps
 where
  handler (n, addrs) addr p
    | n == 0    = (0, addrs)
    | not $ p ^. piInUseL || p ^. piDroppedL = (n - 1, addr : addrs)
    | otherwise = (n, addrs)

-- | Given PeerInfo structure open a socket
openSocket :: (MonadReader TorrentEnv m, MonadIO m) => SockAddr -> m Socket
openSocket addr = do
  env <- getEnv
  atomically $ modifyTVar' (env ^. connectedPeersL) (+ 1)
  s <- liftIO $ do
    s <- socket AF_INET Stream defaultProtocol
    connect s addr
    return s
  peers <- view peersL
  atomically $ modifyTVar' peers $ H.adjust (set piSocketL (Just s)) addr
  return s

-- | Close peer connection setting connected status to false
closeSocket
  :: (MonadIO m, MonadReader TorrentEnv m) => SockAddr -> Socket -> m ()
closeSocket addr s = do
  env   <- getEnv
  peers <- view peersL
  let connected = env ^. connectedPeersL
  atomically $ do
    modifyTVar' peers
      $ H.adjust (set piSocketL Nothing . set piInUseL False) addr
    modifyTVar' connected (\x -> x - 1)
  liftIO $ close s

-- | Starts a peer process given an network address
connectPeer
  :: (MonadUnliftIO m, MonadIO m, MonadReader TorrentEnv m)
  => SockAddr
  -> m (Async ())
connectPeer addr = async $ do
  logInfo $ "Connecting to peer " <> displayShow addr
  env    <- ask
  finSig <- newEmptyMVar
  bracket (openCatch addr) (closeSocket addr) $ \s -> do
    let peerProc =
          runPeer env finSig Nothing s addr (outHandshake >> startPeer)
    peerProc `catch` \(SomeException e) -> do
      logWarn
        $  "Dropped connection with peer ("
        <> displayShow addr
        <> "): "
        <> displayShow e
      peers <- view peersL
      atomically . modifyTVar' peers . H.adjust (set piDroppedL True) $ addr
      throwIO e

openCatch :: (MonadUnliftIO m, MonadReader TorrentEnv m) => SockAddr -> m Socket
openCatch addr = openSocket addr `catch` \(e :: IOException) -> do
  env <- ask
  atomically $ do
    modifyTVar' (env ^. connectedPeersL) (\x -> x - 1)
    modifyTVar' (env ^. peersL)
      . H.adjust (set piInUseL False . set piDroppedL True)
      $ addr
  logWarn
    $  "Failed to connect to peer ("
    <> displayShow addr
    <> "): "
    <> displayShow e
  throwIO e

listenForPeers :: (MonadReader TorrentEnv m, MonadUnliftIO m, MonadIO m) => m ()
listenForPeers = do
  logInfo "Listening for incoming peers"
  s <- listenForPeer
  logDebug $ "Got socket" <> displayShow s
  -- TODO Insert logic here
  listenForPeers

listenForPeer :: (MonadReader TorrentEnv m, MonadUnliftIO m, MonadIO m) => m ()
listenForPeer = do
  addr <- resolve
  bracket (listenCatch addr) (liftIO . close) logic
 where
  resolve = do
    let hints =
          defaultHints { addrFlags = [AI_PASSIVE], addrSocketType = Stream }
    addrs <- liftIO $ getAddrInfo (Just hints) Nothing (Just "6881")
    case headMaybe addrs of
      Just addr -> return addr
      Nothing   -> error "No connection?"
  listenCatch addr = do
    s <- liftIO $ do
      s <- socket (addrFamily addr) (addrSocketType addr) (addrProtocol addr)
      setSocketOption s ReuseAddr 1
      bind s (addrAddress addr)
      let fd = fdSocket s
      setCloseOnExecIfNeeded fd
      return s
    liftIO $ listen s 5
    return s
  logic s = forever $ do
    (s', addr) <- liftIO $ accept s
    peers      <- view peersL
    connected  <- view connectedPeersL
    atomically $ do
      modifyTVar' peers
        $ H.adjust (set piSocketL Nothing . set piInUseL False) addr
      modifyTVar' connected (\x -> x - 1)
    logDebug $ "Peer connected: " <> displayShow addr <> " " <> displayShow s'
    env    <- ask
    finSig <- newEmptyMVar
    let peerProc =
          runPeer env finSig Nothing s' addr (inHandshake >> startPeer)
    peerProc

-- | Close all connections in a torrent environment
closeConnections :: (HasTorrentEnv env, MonadReader env m, MonadIO m) => m ()
closeConnections = do
  peersT <- view $ envL . peersL
  peers  <- atomically $ do
    peers <- readTVar peersT
    writeTVar peersT H.empty
    return peers
  mapM_ (traverse (liftIO . close) . view piSocketL) peers
