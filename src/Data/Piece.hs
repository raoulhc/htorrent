{-|
Module      : Data.Piece
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Contains data structures for piece information and methods to write these to
file.
-}

module Data.Piece
  ( PieceLoc(..)
  , PieceState(..)
  , PieceInfo(..)
  )
where

import           RIO

import           Data.MetaInfo

-- | Data holding a pieces index and offset
data PieceLoc = PieceLoc Word32 Word32 deriving (Eq, Show)

-- | Possible piece states used for shared states between peer threads
data PieceState
  = Got
  | Lack
  | Downloading Word32 -- ^ How many blocks left
  deriving (Eq, Show)

-- | All information needed to write a payload to a file
data PieceInfo = PieceInfo
  FilePath                   -- ^ Root filepath
  ByteString                 -- ^ Relative filepath
  (Either Word32 [FileInfo]) -- ^ Single or multiple file info
  Word32                     -- ^ Absolute offset
  deriving (Eq, Show)
