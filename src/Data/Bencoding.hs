{-|
Module      : Data.Bencoding
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

This contains the bencoding data structure, prism methods on that structure and
data serialisation methods to and from bytestrings.
-}

{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Data.Bencoding
  ( Bencoding(..)
  , key
  , _BStr
  , _BInt
  , _BList
  , _BDict
  , bStr
  , bInt
  , bDict
  , bList
  )
where

import           RIO
import qualified RIO.Map                       as M
import qualified RIO.Vector                    as V
import           RIO.Partial
import           Control.Lens                   ( makePrisms
                                                , ix
                                                )
import qualified Data.ByteString.Char8         as B
import           Data.Char                      ( chr
                                                , isDigit
                                                , ord
                                                )
import           Data.Serialize

-- | Recursive structure with the 4 possible types of data, bytestring, integer,
--   dictionary and list
data Bencoding = BStr !ByteString
               | BInt !Word32
               | BDict !(Map ByteString Bencoding)
               | BList !(Vector Bencoding)
               deriving (Eq, Show)

makePrisms ''Bencoding

-- | Useful auxiliary function when looking at keys of a dict
key
  :: Applicative f
  => ByteString
  -> (Bencoding -> f Bencoding)
  -> Bencoding
  -> f Bencoding
key k = _BDict . ix k

fromW8 :: Word8 -> Char
fromW8 = chr . fromIntegral

toW8 :: Char -> Word8
toW8 = fromIntegral . ord

char :: Char -> Get Char
char x = do
  w <- getWord8
  if fromW8 w == x
    then return x
    else fail $ "Expected: '" <> show x <> "', got: '" <> [fromW8 w] <> "'"

digit :: Get Char
digit = do
  w <- fromW8 <$> getWord8
  if isDigit w
    then return w
    else fail $ "Expected digit, got: " <> show w <> "."

many1 :: Get a -> Get [a]
many1 g = liftA2 (:) g (many g)

bInt :: Get Bencoding
bInt = do
  char 'i'
  i <- many1 digit
  char 'e'
  return . BInt . read $ i

bStr :: Get Bencoding
bStr = do
  i <- read <$> many1 digit
  char ':'
  BStr <$> getBytes i

bList :: Get Bencoding
bList = do
  char 'l'
  b <- V.fromList <$> many1 bValue
  char 'e'
  return $ BList b

bDict :: Get Bencoding
bDict = do
  char 'd'
  dic <- many1 $ do
    BStr k <- bStr
    v      <- bValue
    return (k, v)
  char 'e'
  return . BDict . M.fromList $ dic

bValue :: Get Bencoding
bValue = do
  w <- fromW8 <$> lookAhead getWord8
  if
    | w `B.elem` "i" -> bInt
    | w `B.elem` "l" -> bList
    | w `B.elem` "d" -> bDict
    | otherwise      -> bStr

wrap :: Char -> Char -> Put -> Put
wrap o c p = do
  putWord8 (toW8 o)
  p
  putWord8 (toW8 c)

putShow :: Show a => a -> Put
putShow = mapM_ put . show

putBValue :: Bencoding -> Put
putBValue (BInt i) = wrap 'i' 'e' $ putShow i
putBValue (BStr bs) =
  putShow (B.length bs) >> putWord8 (toW8 ':') >> putByteString bs
putBValue (BList l) = wrap 'l' 'e' $ mapM_ put l
putBValue (BDict d) =
  wrap 'd' 'e' $ mapM_ (\(x, y) -> put (BStr x) >> put y) (M.toList d)

instance Serialize Bencoding where
  put = putBValue
  get = bValue
