{-|
Module      : Data.App
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

The App data structure used at the base of htd
-}
module Data.App where

import           RIO
import           RIO.Process

import           Data.Config
import           Data.Logging
import           Utils

data App = App
  { _logFunc :: !HLog
  , _processCtx :: !ProcessContext
  , _config :: !Config
  , _logOpts :: !HLogOpts
  }
  -- Add more stuff as it comes along

makeLenses ''App

instance HasHLog App where
  hLogL = lens _logFunc (\x y -> x { _logFunc = y })

instance HasProcessContext App where
  processContextL = lens _processCtx (\x y -> x { _processCtx = y })
