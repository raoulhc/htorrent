module Data.Config
  ( Config(..)
  , getConfig
  , readConfig
  , ensureDirectories
  , ConfigBuilder(..)
  , cfgSessionPathL
  , cfgDownloadPathL
  , cfgLoggingPathL
  , cfgVerboseL
  )
where

import           RIO                     hiding ( logDebug )
import           RIO.Directory
import           RIO.Partial                    ( fromJust )
import           RIO.FilePath
import           Data.Monoid
import           Data.Yaml

import           AppOptions
import           Data.Logging
import           Utils

-- | Structure to store all necessary configuration
data Config = Config
  { _cfgSessionPath  :: !FilePath
  , _cfgDownloadPath :: !FilePath
  , _cfgLoggingPath  :: !(Maybe FilePath)
  , _cfgVerbose      :: !Bool
  } deriving (Show, Eq)

makeLenses ''Config

-- | Intermediate config to combine preferences
data ConfigBuilder = ConfigBuilder
  { _cbSession :: !(First FilePath)
  , _cbDownload :: !(First FilePath)
  , _cbLogging :: !(First FilePath)
  , _cbVerbose :: !(First Bool)
  } deriving (Eq, Show)

instance Semigroup ConfigBuilder where
  ConfigBuilder w x y z <> ConfigBuilder w' x' y' z' =
    ConfigBuilder (w <> w') (x <> x') (y <> y') (z <> z')

instance FromJSON ConfigBuilder where
  parseJSON (Object v) =
    ConfigBuilder
      <$> (First <$> v .:? "session")
      <*> (First <$> v .:? "downloads")
      <*> (First <$> v .:? "logging")
      <*> (First <$> v .:? "verbose")
  parseJSON _ = fail "Expected object"

-- Probably want to change this to Either later to get errors that make sense
buildConfig :: ConfigBuilder -> Maybe Config
buildConfig b = do
  session  <- getFirst $ _cbSession b
  download <- getFirst $ _cbDownload b
  let logging = getFirst $ _cbLogging b
      verbose = fromMaybe False . getFirst $ _cbVerbose b
  pure $ Config session download logging verbose

-- | Combines configs in order of preference, flags > provided config > config >
--   defaults, will currently always return a config
getConfig :: MonadIO m => AppOptions -> m (Maybe Config)
getConfig app = do
  let flags = Just $ appBuilder app
  localCfg <- case app ^. aConfigL of
    Just fp -> readConfig fp
    Nothing -> pure mempty
  global   <- defaultConfigPath >>= readConfig
  defaults <- Just <$> defaultConfigBuilder
  case buildConfig $ fromJust $ flags <> localCfg <> global <> defaults of
    Just (Config w x y z) -> do
      home <- getHomeDirectory
      pure . pure $ Config (expandHome home $ normalise w)
                           (expandHome home $ normalise x)
                           (fmap (expandHome home . normalise) y)
                           z
    Nothing -> pure Nothing

appBuilder :: AppOptions -> ConfigBuilder
appBuilder app = ConfigBuilder (First $ app ^. sessionDirL)
                               (First Nothing)
                               (First Nothing)
                               (First $ app ^. verboseL)

ensureDirectories
  :: HasHLog env => MonadReader env m => MonadIO m => Config -> m ()
ensureDirectories (Config w x y _) =
  let files = [w, x] <> maybe mempty pure y
  in  logDebug "Creating directories if needed"
        *> mapM_ (createDirectoryIfMissing True) files

-- | Try to read config
readConfig :: MonadIO m => FilePath -> m (Maybe ConfigBuilder)
readConfig fp =
  (\case
      Right cfg -> Just cfg
      Left  _   -> Nothing
    )
    <$> (liftIO . decodeFileEither $ fp)

defaultConfigPath :: MonadIO m => m FilePath
defaultConfigPath = (</> ".htd" </> "config") <$> getHomeDirectory

defaultSession :: MonadIO m => m FilePath
defaultSession = (</> ".htd" </> "session") <$> getHomeDirectory

defaultDownload :: MonadIO m => m FilePath
defaultDownload = (</> "Downloads") <$> getHomeDirectory

defaultConfigBuilder :: MonadIO m => m ConfigBuilder
defaultConfigBuilder =
  ConfigBuilder
    <$> (First . Just <$> defaultSession)
    <*> (First . Just <$> defaultDownload)
    <*> pure (First Nothing)
    <*> pure (First $ Just False)
