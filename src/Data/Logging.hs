module Data.Logging where

import           RIO                     hiding ( logGeneric )
import           RIO.List
import           RIO.Time

import           Text.Printf

import           Data.ByteString.Builder        ( charUtf8
                                                , stringUtf8
                                                )
import           Data.ByteString.Builder.Extra  ( flush )

import           Utils

-- | Options to create a log function from
data HLogOpts = HLogOpts
  { _context :: ![(Builder, String)]                 -- ^ List of colours and strings
  , _format :: !([(Builder, String)] -> Utf8Builder) -- ^ Function to formats these together
  , _logLevel :: !LogLevel                           -- ^ Log level to begin logging
  , _logSend :: !(forall m. MonadIO m
    => HLogOpts -> LogLevel -> Utf8Builder -> m ())  -- ^ method to print logs to IO
  }

-- terminal colour codes

reset, black, red, green, yellow, blue, magenta, cyan, white :: Builder
reset = "\ESC[0m"
black = "\ESC[90m"
red = "\ESC[31m"
green = "\ESC[32m"
yellow = "\ESC[33m"
blue = "\ESC[34m"
magenta = "\ESC[35m"
cyan = "\ESC[36m"
white = "\ESC[37m"

makeLenses ''HLogOpts

newtype HLog = HLog
  { runHLog :: forall m. MonadIO m => LogLevel -> Utf8Builder -> m ()
  }

-- Class for how to get a log function from an environment
class HasHLog env where
  hLogL :: Lens' env HLog

-- | Generate a hlog to
withHLog :: MonadIO m => HLogOpts -> (HLog -> m a) -> m a
withHLog lo@(HLogOpts _ _ _ logSend) logic = logic $ HLog (logSend lo)

-- | Standard options, easy to build on top of this
defaultOpts :: HLogOpts
defaultOpts = HLogOpts
  { _context  = []
  , _format   = squareFormat
  , _logLevel = LevelDebug
  , _logSend  = \lo ll builder -> when (ll >= lo ^. logLevelL) $ do
                  time <- getUtf8Builder <$> timestamp
                  let level = getUtf8Builder $ displayLevel ll
                      ctx   = getUtf8Builder . (lo ^. formatL) $ lo ^. contextL
                  hPutBuilder stdout
                    $  time
                    <> " "
                    <> level
                    <> " "
                    <> ctx
                    <> " "
                    <> getUtf8Builder builder
                    <> "\n"
                    <> flush
  }

-- | Format contexts in squares truncating longer names
squareFormat :: [(Builder, String)] -> Utf8Builder
squareFormat = mconcat . intersperse " " . map
  (\(c, x) ->
    Utf8Builder $ c <> charUtf8 '[' <> nameCutoff x <> charUtf8 ']' <> reset
  )

-- | Cuts off end of string to line up formatting
nameCutoff :: String -> Builder
nameCutoff s =
  stringUtf8
    $ let len = length s
      in  if len > 21 then take 19 s <> ".." else printf "%21s" s

logGeneric
  :: (MonadReader env m, HasHLog env, MonadIO m)
  => LogLevel
  -> Utf8Builder
  -> m ()
logGeneric ll bs = do
  lf <- view hLogL
  runHLog lf ll bs

displayLevel :: LogLevel -> Utf8Builder
displayLevel = \case
  LevelDebug   -> Utf8Builder $ green <> stringUtf8 "[Debug]" <> reset
  LevelInfo    -> Utf8Builder $ blue <> stringUtf8 "[Info ]" <> reset
  LevelWarn    -> Utf8Builder $ yellow <> stringUtf8 "[Warn ]" <> reset
  LevelError   -> Utf8Builder $ red <> stringUtf8 "[Error]" <> reset
  LevelOther _ -> error "Should be unreachable"

logDebug, logInfo, logError, logWarn
  :: (MonadReader env m, HasHLog env, MonadIO m) => Utf8Builder -> m ()
logDebug = logGeneric LevelDebug
logInfo = logGeneric LevelInfo
logWarn = logGeneric LevelWarn
logError = logGeneric LevelError

timestamp :: MonadIO m => m Utf8Builder
timestamp = do
  now <- getZonedTime
  return
    .  Utf8Builder
    $  black
    <> "["
    <> fromString (formatTime' now)
    <> "]"
    <> reset
  where formatTime' = formatTime defaultTimeLocale "%F %T"
