{-|
Module      : Data.MetaInfo
Copyright   : (c) Raoul Hidalgo Charman, 2019
License     : BSD
Maintainer  : raoul.hidalgo.charman@gmail.com
Stability   : experimental

Contains MetaInfo and related data structures as well as methods to access and
verify this information.
-}

{-# OPTIONS_GHC -Wno-unused-do-bind #-}

module Data.MetaInfo where

import           RIO
import           RIO.Partial
import qualified RIO.ByteString                as B
import           Control.Lens                   ( (^?)
                                                , (^?!)
                                                , ix
                                                )
import qualified Data.Serialize                as S
import           Data.Either.Combinators        ( maybeToRight )
import           Crypto.Hash.SHA1

import           Data.Bencoding
import           Utils                          ( makeLenses )

-- | Data structure for each individual file
data FileInfo = FileInfo
    { _fileLength :: Word32
    , _path :: [ByteString]
    , _md5sum :: Maybe ByteString
    } deriving (Eq, Show)

makeLenses ''FileInfo

-- | Data structure containing info dictionary
data InfoDict = InfoDict
    { _name :: ByteString
    , _pieceLength :: Word32
    , _pieces :: ByteString
    , _files :: Either Word32 [FileInfo]
    } deriving (Eq, Show)

makeLenses ''InfoDict

-- | Data structure containing the Bencoding and info hash
data MetaInfo = MetaInfo
    { _announce :: ByteString
    , _announceList :: Maybe [[ByteString]]
    , _infoDict :: InfoDict
    , _pieceNumber :: Word32
    , _totalSize :: Word32
    , _infoHash :: ByteString
    } deriving (Eq, Show)

makeLenses ''MetaInfo

-- | Take a file and read into MetaInfo structure
readTorrent :: MonadIO m => FilePath -> m (Either String MetaInfo)
readTorrent fp = do
  b <- B.readFile fp
  let p = S.decode b
  return $ p >>= verifyMetaInfo

-- | Verify a meta info file and create MetaInfo structure if so
verifyMetaInfo :: Bencoding -> Either String MetaInfo
verifyMetaInfo b = do
  a <- case b ^? key "announce" . _BStr of
    Just x  -> Right x
    Nothing -> Left "Announce url doesn't exist!"
  let alist = getAnnounceList b
  infodict <- case b ^? key "info" of
    Just x  -> verifyInfoDict x
    Nothing -> Left "Info dictionary doesn't exist!"
  let infohash  = hash . S.encode $ (b ^?! key "info")
      pieceNum  = toEnum . (`div` 20) . B.length $ _pieces infodict
      totalSize = case infodict ^. filesL of
        Right files -> foldl' (\acc (FileInfo len _ _) -> acc + len) 0 files
        Left  size  -> size
  return $ MetaInfo a alist infodict pieceNum totalSize infohash

-- | Verify the info dict within a metainfo file
verifyInfoDict :: Bencoding -> Either String InfoDict
verifyInfoDict b = do

  n <- case b ^? key "name" . _BStr of
    Just x  -> Right x
    Nothing -> Left "Name doesn't exist"

  ps <- case b ^? key "pieces" . _BStr of
    Just x -> if B.length x `mod` 20 == 0
      then Right x
      else Left "pieces not a multiple of 20"
    Nothing -> Left "Pieces doesn't exist"

  pLen <- case b ^? key "piece length" . _BInt of
    Just x  -> Right x
    Nothing -> Left "piece length doesn't exist!"

  fs <- case b ^? key "length" of
    Just x -> case x ^? _BInt of
      Just y  -> Right $ Left y
      Nothing -> Left "Length did not contain a valid Int"
    Nothing -> case b ^? key "files" of
      Just x -> case verifyFiles x of
        Right y -> Right $ Right y
        Left  y -> Left $ "couldn't verify list of files: " <> y
      Nothing -> Left "Neither length nor files found"

  return $ InfoDict n pLen ps fs

-- | Verify the files listed in the info dict are valid
verifyFiles :: Bencoding -> Either String [FileInfo]
verifyFiles b = do
  rawFiles <- maybeToRight "files key not a list structure!" (b ^? _BList)
  maybeToRight "Unable to verify files" . fmap toList $ traverse verifyFile
                                                                 rawFiles
 where
  verifyFile x = do
    l <- x ^? key "length" . _BInt
    p <- toList . fmap (^. _BStr) <$> x ^? key "path" . _BList
    let md5 = x ^? key "md5sum" . _BStr
    return $ FileInfo l p md5

-- | Get announce list from meta info file
getAnnounceList :: Bencoding -> Maybe [[B.ByteString]]
getAnnounceList b = do
  al <- toList <$> b ^? key "announce-list" . _BList
  a  <- traverse (^? _BList) al
  (traverse . traverse) (^? _BStr) (fmap toList a)

-- | get the info hash (SHA1 of bencoding info dictionary) out of
--   a metainfo structure
getInfoHash :: Bencoding -> Maybe B.ByteString
getInfoHash b = hash . S.encode <$> (b ^? _BDict . ix "info")
