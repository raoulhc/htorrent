# htd

htd (Haskell Torrent Daemon) is my attempt at making a torrent client in Haskell.

Inspired by rtorrent, I plan to have a server/client architecture similar to
MPD (music player daemon) allowing different interfaces and not needing to
wrap it with screen/dtach.

## To do

- [x] parse bencoding
- [ ] read in torrent files
    - [x] read in files into a MetaInfo structure
    - [x] verify announce url
    - [x] verify info
    - [ ] verify paths
- [ ] tracker protocol requests
    - [x] http tracker requests
- [ ] Download file from peers
    - [x] Add torrent states structure
      * Uses transactional variables in a Reader monad.
    - [x] torrent handshake
    - [x] Data stream
        - [x] Basic messages (choke/unchoke/interested/not interested)
        - [x] Piece messages (have/bitfield/request)
        - [x] Piece Payloads
    - [ ] Logic for tie together above 
- [ ] add concurrency/parallelism so multiple files can be downloaded
    - [ ] peer thread structure
    - [ ] torrent thread structure
    - [ ] multiple torrent structure
- [ ] make this run as a daemon
- [ ] create API to interface the daemon
- [ ] create CLI that uses said interface

### extra stuff

There are a bunch of beps that would be good to get but aren't necessary. 

- [ ] udp tracker requests
- [ ] dht requests
- [ ] private torrents
- [ ] fast exchange
- [ ] magnet links
- [ ] announce list
- [ ] local service discovery
- [ ] utorrent transport protocol
